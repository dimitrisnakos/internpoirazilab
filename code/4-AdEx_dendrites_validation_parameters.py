#!/usr/bin/env python3

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###### Parameters #######

## experiment time ##
duration_no_current = 300 * ms
duration_current = 1000 * ms

## geometry of neurons ##

# Dendrite
d_d = 1.0 * um # diametre of dendrite
l_d = 1743 * um # length of dendrite
area_d = pi * d_d * l_d # total area of dendrite (tube)
g_Ld = 0.00001 * siemens/cm**2 * area_d
E_Ld = -87. * mV
Cd = 2.5 * uF/cm**2 * area_d

# Soma
d_s = 10. * um # diametre of soma
l_s = 10. * um # length of soma
area_s = pi * d_s * l_s # total area of soma (sphere)
g_Ls = 0.00003 * siemens/cm**2 * area_s
E_Ls = -82. * mV
Cs = 1. * uF/cm**2 * area_s

tw = Cs / g_Ls
print (tw)

# Global parameters
alpha = 2. * nS
beta = 0.0045 * nA #0.0045
tauw = 45. * ms
v_thres = 0. * mV
v_reset = - 74. * mV 
delta_T = 2. * mV
v_T = -56. * mV
gc = 10 * nS # coupling conductance for first dendrite
Iinj = 0.1 * nA

####### Model #######
eqs = '''
dvd/dt = (-g_Ld * (vd - E_Ld) - gc * (vd - vm) + I_d )/ Cd : volt
dvm/dt = (- g_Ls * (vm - E_Ls) + g_Ls * delta_T * exp((vm - v_T) / delta_T) - gc * (vm - vd) -w + I_s) / Cs : volt (unless refractory)
dw/dt = (alpha * (vm - E_Ls) - w) / tauw : amp
I_s : amp
I_d : amp
'''

neuron = NeuronGroup(1, model = eqs, threshold = 'vm > v_thres', reset = "vm = v_reset; w += beta", refractory = 2 * ms, method = "euler")

neuron.vm = E_Ls
neuron.vd = E_Ld

trace = StateMonitor(neuron, ('vm', 'vd'), record = 0)
spk = SpikeMonitor(neuron)

run(duration_no_current)
neuron.I_s = Iinj
run(duration_current)
neuron.I_s = 0 * nA
run(duration_no_current)

# Nicer spikes (as per example of Gersner in brian2)
vm = trace[0].vm[:]
for t in spk.t:
    i = int(t / defaultclock.dt)
    vm[i] = 20*mV

####### Validation #######

#Iinj = 0.2

### Positive current ###
#rhebase = 0.08 * nA # minimum current to produce one spike 
#V-I curve = from -400*pA to the rheobase
#f_I curve = I_s from 0 to 1 nA
# duration of current == 300 - 1300

#print('Spike frequency with', Iinj, 'is', spk.num_spikes / duration_current)
Vpeak= ( vm[12290] - min(vm) )
print(' Vpeak with', Iinj, 'is', round(Vpeak/mV, 2 ) * mV )

### Negative Current ###
## Rinput ##
#DV = vm[12990] / mV - vm[2990] / mV
#Rin = (DV * mV) / Iinj

#print('R input is:', Rin)

## Sag ratio ##
#sag = DV / ( min(vm) / mV - vm[2990] / mV ) 

#print('sag ratio is:', sag)

####### Plots #######
plt.plot(trace.t / ms, vm / mV)
plt.plot(trace.t / ms, trace[0].vd[:] / mV)
#plt.plot(Iinj, spk)
#plt.plot(Vpeak, Inj)
#plt.ylim([-90, 22])
plt.xlabel('time (ms)')
plt.ylabel('membrane potential (mV)')
plt.legend(['soma', 'apical trunk'])
plt.show();