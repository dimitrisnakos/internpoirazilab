#!/usr/bin/env python3


####### HH parametric exploration for #######
####### active dendrites Na spikes    #######

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###### Parameters #######

## experiment time ##
duration_no_current = 300 * ms
duration_current = 1 * ms

## geometry of neurons ##

# Dendrite
d_d = 1.0 * um # diametre of dendrite
l_d = 1743 * um # length of dendrite
area_d = pi * d_d * l_d # total area of dendrite (tube)
g_Ld = 0.00001 * siemens/cm**2 * area_d
E_Ld = -82. * mV
Cd = 2.5 * uF/cm**2 * area_d

# Soma
d_s = 10. * um # diametre of soma
l_s = 10. * um # length of soma
area_s = pi * d_s * l_s # total area of soma (sphere)
g_Ls = 0.00003 * siemens/cm**2 * area_s
E_Ls = -87. * mV
Cs = 1. * uF/cm**2 * area_s

tw = Cs / g_Ls

# Global parameters
E_Na = 50. * mV
E_K = 12. * mV
g_Na = 1.2 * nS #msiemens/cm ** 2
g_K = 1.2 * nS
alpha = 2. * nS
beta = 0.0045 * nA #0.0045
tauw = 45. * ms
v_thres = 0. * mV
v_reset = - 74. * mV 
delta_T = 2. * mV
v_T = -56. * mV
gc = 10 * nS # coupling conductance for first dendrite
Iinj = 0.1 * nA

####### Model #######
eqs = '''
dvm/dt = (- g_Ls * (vm - E_Ls) + g_Ls * delta_T * exp((vm - v_T) / delta_T) - gc * (vm - vd) -w + I_s) / Cs : volt (unless refractory)
dw/dt = (alpha * (vm - E_Ls) - w) / tauw : amp

dvd/dt = (-g_Ld * (vd - E_Ld) - gc * (vd - vm) + I_d + I_Na + I_K)/ Cd : volt


I_Na = -g_Na * m**3 * h * (vd - E_Na) : amp

dm/dt = a_m * (1 - m) - b_m * m : 1

a_m = (0.1 / mV) * (-vd + 25 * mV) / (exp((-vd + 25 * mV) / (10 * mV)) - 1) / ms : Hz
b_m = 0.1 * exp(-vd / (18 * mV)) / ms : Hz

dh/dt = a_h * (1 - h) - b_h * h : 1

a_h = 0.07 * exp(-vd / (20 * mV)) / ms : Hz
b_h = 1 / (exp((-vd + 30 * mV) / (10 * mV)) + 1)/ms : Hz


I_K = -g_K * n**4 * (vd - E_K) : amp

dn/dt = a_n * (1-n) - b_n * n : 1

a_n = (0.01 / mV) * ( -vd + 10 * mV) / (exp((-vd + 10 * mV) / (10 * mV)) - 1) / ms : Hz
b_n = 0.125 * exp( -vd / (80 * mV)) / ms : Hz

I_s : amp
I_d : amp
'''

neuron = NeuronGroup(1, model = eqs, threshold = 'vm > v_thres', reset = "vm = v_reset; w += beta", refractory = 2 * ms, method = "euler")

neuron.vm = E_Ls
neuron.vd = E_Ld

trace = StateMonitor(neuron, ('vm', 'vd', 'm', 'h', 'n'), record = 0)
spk = SpikeMonitor(neuron)

run(duration_no_current)
neuron.I_d = Iinj
run(duration_current)
neuron.I_d = 0 * nA
run(duration_no_current)

# Nicer spikes (as per example of Gersner in brian2)
vm = trace[0].vm[:]
for t in spk.t:
    i = int(t / defaultclock.dt)
    vm[i] = 20*mV

# Plots
plt.plot(trace.t / ms, vm / mV)
plt.plot(trace.t / ms, trace[0].vd[:] / mV)

plt.xlabel('time (ms)')
plt.ylabel('membrane potential (mV)')
plt.legend(['soma', 'apical trunk'])
plt.show();
