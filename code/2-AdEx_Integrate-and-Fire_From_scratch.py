#!/usr/bin/env python3

####### From Scratch model of AdEx #######

# Based on "Adaptive Exponential Integrate-and-Fire Model 
# as an Effective Description of Neuronal Activity" Gerstner

import matplotlib.pyplot as plt
import numpy as np

# Variables
totdur = 10000          # how many _ms_ I insert current in total
dt = 0.10               # how many _ms_ does each current last
E_l = -58.              # resting potential in mV (leak current)
I_exc = 400.            # excitatory current in mA
I_inh = 20.             # inhibitory current in mA
V_th = -50.             # spike threshold 
V_reset = -50.          # reset voltage
g_l = 18.               # leak conductance
Dt = 2.                 # slope factor mV

#----- These parameters == qualitative changes----------#
tm = 150.               # adaptation time constant
C = 130.                # dendrite capaciatance
a = 4.                  # voltage coupling (sensitivity of adaptation current to voltage == linearized channels)
b = 120.                # spike triggered adaptation

steps = int(totdur/dt)  # How many times Euler method runs
V = np.empty([steps,1]) # empty array to store all voltages
w = np.empty([steps,1]) # array for adaptation current w

V[0] = E_l              # set initial memenbrane potential value
w[0] = 0                # set initial adaptation value 
spk = [0]               # vector for spikes. 0 needed to loop in line 30.

# Euler method
for n in range(steps-1):
    
    V[n+1] = V[n] + (dt/C) * ((-(g_l) * ((V[n] - E_l)) + (g_l) * Dt * np.exp((V[n]-V_th)/Dt) + I_exc - w[n]))
    w[n+1] = w[n] + (dt/tm) * (a * (V[n] - E_l) - w[n])

    if V[n+1] >= V_th and (n+1-spk[-1]) > int(2./dt):
        V[n+1] = V_reset
        w[n+1] = w[n+1] + b
        spk.append(n+1) # keep points at which we have spikes

V[spk]=20 #Added to make spikes more realistic and reach the threshold of +20mV.
plt.plot(V)
plt.xlabel('steps')
plt.ylabel('voltage (mV)')
plt.show(); # Used for running on terminal.
