#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

####### From Scratch Integrate-and-Fire #######
####### model of a neuron (not simple)  #######

# All values taken from Chavlis et al Dendrites of DG

# Variables
totdur = 10000          # how many _ms_ I insert current in total
dt = 0.10               # how many _ms_ does each current last
steps = int(totdur/dt)  # How many times Euler method runs
E_l = -87.0             # resting potential in mV
tm = 45.0               # tau in ms
I_ext = 60.0            # external current in mA
V_th = -56.0            # spike threshold
V_reset = -74.0         # reset voltage

V = np.empty([steps,1]) # empty array to store all voltages

V[0] = E_l              # set initial memenbrane potential value
spk = [0]               # vector for spikes. 0 needed to loop in line 34.

n1 = int(1000./dt)
n2 = int(3000./dt)

# Euler method loop 
for n in range(steps-1): # -1 because V[last value] is redundant, since program starts from 0
    
    V[n+1] = V[n] + dt * (-(1/tm) * ((V[n] - E_l) - I_ext))
    
    if V[n+1] >= V_th and (n+1-spk[-1]) > int(2./dt):
        V[n+1] = V_reset
        spk.append(n+1) # keep points at which we have spikes
 

# specify different current values 
    if n >= n1 and n <= n1 + int(1000/dt):
        I_ext = 32.
    
    elif n >= n2 and n <= n2 + int(1000/dt):
        I_ext = -20.
    
    else :
        I_ext = 0.


spk_rt = [i * dt for i in spk] # spikes in times

V[spk[1:]] = 20 # Used in order to artificially create spikes that reach a threshold of +20 mV. spk[1:] used in order to avoid one initial line that hit +20 mV that we didn't want.

plt.plot(V)
plt.xlabel('steps')
plt.ylabel('voltage (mV)')
plt.show(); # Used for running on terminal.
