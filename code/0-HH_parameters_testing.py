#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

vd = np.linspace(-100, 50, num = 1000)

###### Equation parameters ######

a_m1 = 50.
a_m2 = -10.
a_m3 = 10.

b_m1 = 0.01
b_m2 = -10.
b_m3 = 0.09

a_h1 = 0.001
a_h2 = -7.
a_h3 = 0.19

b_h1 = 100.
b_h2 = -10.
b_h3 = 2.

a_n1 = 50.
a_n2 = 15.
a_n3 = 2.

b_n1 = 0.005
b_n2 = -50.
b_n3 = 0.09

###### Equations ######
a_m = a_m1 * (-vd + a_m2) / (np.exp((-vd + a_m2) / (a_m3)) - 1)
b_m = b_m1 * np.exp( -vd / b_m2)

a_h = a_h1 * np.exp(-vd / a_h2)
b_h = b_h1 / (np.exp((-vd + b_h2) / b_h3) + 1)

a_n = 0.01 * ( -vd + 10 ) / (np.exp(( -vd + 10 ) /  10) - 1)
b_n = 0.125 * np.exp(-vd / 80)

plt.close('all')

plt.figure(1)
plt.subplot(231)
plt.plot(vd, a_m)
plt.legend(['a_m'])

plt.subplot(234)
plt.plot(vd, b_m)
plt.legend(['b_m'])

plt.subplot(232)
plt.plot(vd, a_h)
plt.legend(['a_h'])

plt.subplot(235)
plt.plot(vd, b_h)
plt.legend(['b_h'])

plt.subplot(233)
plt.plot(vd, a_n)
plt.legend(['a_n'])

plt.subplot(236)
plt.plot(vd, b_n)
plt.legend(['b_n'])

m00 = a_m / (a_m + b_m)
h00 = a_h / (a_h + b_h)
n00 = a_n / (a_n + b_n)

tm = 1/(a_m + b_m)
th = 1/(a_h + b_h)
tn = 1/(a_n + b_n)

plt.figure(2)
plt.subplot(121)
plt.plot(vd, m00); plt.plot(vd, h00); plt.plot(vd, n00)
plt.legend(['m00', 'h00', 'n00'])

plt.subplot(122)
plt.plot(vd, tm); plt.plot(vd, th); plt.plot(vd, tn)
plt.legend(['tm', 'th', 'tn'])