#!/usr/bin/env python3

######################################################
#                                                    #
#   Code taken from Brete and used for parametric    #
#     exploration of HH model in order to create     #
#              dendritic sodium spikes.              #
#         No AdEx Integrate-and-fire here.           #
#                                                    #
######################################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as np

### neuronal Parameters ###
area    = 20000*umetre**2
Cm      = (1*ufarad*cm**-2) * area
gl      = 1 * (5e-5*siemens*cm**-2) * area
gc      = 0.01*siemens*cm**-2 * umetre **2 # 1 * (5e-5*siemens*cm**-2) * area # goes with 'threshold = vs > *'

El      = -60*mV    # Changes where the spike graphs starts. Default: -60
EK      = -120*mV   # spike min to lower values.
ENa     = 15*mV      # it designates where the peak will be. In addition with k**2?
g_na    = 0*(100 *msiemens*cm**-2) * area
g_kd    = (30 *msiemens*cm**-2) * area  # spike min to lower values with high g_kd
VT      = -53 * mV  # -53 Doesn't change anything.
v_reset = (-74. + 0) *mV # -74


delta_T = 2. * mV

### Model parameters ###

# Notes on how parameters change: 
# k**2 -> at +2 moves spike to the right. at -{*} increased spike #. 
# k**1 -> controls width/sharpness. ^ values -> sharp. low values -> wide.
# k**3 -> changes frequency of spikes. ^ values -> more spikes.


# K gates 
kan1 = 0.032*(mV**-1) * 1.1
kan2 = (-18 + -10) * mV #Default: 15*mV + VT
kan3 = (5 + -0) *mV 

kbn1 = .5 * 1.1
kbn2 = (-43 + -10) * mV # Default: 10*mV + VT
kbn3 = (40 + -0) * mV

# Na activation 
kam1 = 0.32*(mV**-1) * 1.1
kam2 = (-40 + 12) * mV # Default: 13*mV+VT This is the threshold!!!!!
kam3 = (4 + -0) * mV

kbm1 = 0.28*(mV**-1) * 1.1
kbm2 = (13 + -10) * mV # Default: -VT-40*mV # range of curve?
kbm3 = (5 + -0) * mV

# Na deactivation 
kah1 = 0.128 * 1.1
kah2 = (-16 + -10) * mV # Default: 17*mV+VT
kah3 = (18 + -0) * mV

kbh1 = 4. * 1.1
kbh2 = (-13 + -10) * mV # Default: 40*mV+VT
kbh3 = (5 + -0) * mV

## The model

# Starts working with dendritic spikes @ ~ Iext = 0.15 nA

# In case I want to add AdEx
# dvs/dt = (- gc * (vs - El) + g_Ls * delta_T * exp((vs - vT) / delta_T) - gc * (vs - v) - w / Cm : volt (unless refractory)
# dw/dt = (alpha * (vs - El) - w) / tauw : amp
    
eqs = Equations('''
    dvs/dt = (-gl*(vs-El) - gc*(vs-v)) /Cm : volt 

    dv/dt = (-gl*(v-El) - gc*(v-vs) - INa - IK + Iext)/Cm : volt 
    
    INa = g_na*(m**3)*h*(v-ENa) : amp
    IK = g_kd*(n**4)*(v-EK) : amp

    dm/dt = alpha_m*(1-m)-beta_m*m : 1
    dn/dt = alpha_n*(1-n)-beta_n*n : 1
    dh/dt = alpha_h*(1-h)-beta_h*h : 1


    alpha_n = kan1*(-v+kan2) / (exp((-v+kan2)/(kan3))-1.)/ms : Hz

    beta_n  = kbn1*exp((-v + kbn2)/(kbn3))/ms : Hz


    alpha_m = kam1*(-v+kam2)/(exp((-v+kam2)/(kam3))-1.)/ms : Hz

    beta_m  = kbm1*(v+kbm2)/(exp((v+kbm2)/(kbm3))-1)/ms : Hz


    alpha_h = kah1*exp((-v+kah2)/(kah3))/ms : Hz

    beta_h  = kbh1/(1+exp((-v+kbh2)/(kbh3)))/ms : Hz

    Iext: amp
''')


neuron = NeuronGroup(1, model=eqs, threshold='vs > -30*mV', reset = 'vs = v_reset', refractory=3*ms, method='exponential_euler')

# Initialization
neuron.v   = El
neuron.vs  = El

# Record a few traces

trace = StateMonitor(neuron, ('v','vs','INa','IK'), record = True)#record=[0])
run(10 * ms, report='text')
neuron.Iext = 2.2 * nA
run(5 * ms, report='text')
neuron.Iext = 0.0 * nA
run(10 * ms, report='text')

plt.figure(1)
plt.plot(trace.t/ms, trace[0].v/mV)
plt.plot(trace.t/ms, trace[0].vs/mV)
plt.xlabel('t (ms)')
plt.ylabel('v (mV)')
plt.legend(['dendrite','soma','INa'])

plt.figure(2)
plt.plot(trace.t/ms, trace[0].INa/nA)
plt.plot(trace.t/ms, trace[0].IK/nA)
plt.xlabel('V (mV)')
plt.ylabel('I (nA)')
plt.legend(['INa', 'IK'])
plt.show();