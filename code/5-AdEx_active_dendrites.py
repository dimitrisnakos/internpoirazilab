#!/usr/bin/env python3

####### Code to try to get dendrites to have active Na spikes #######

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###### Parameters #######

## experiment time ##
duration_no_current1 = 30 * ms
duration_no_current2 = 300 * ms
duration_current = 10 * ms

## geometry of neurons ##

# Dendrite
d_d     = 1.0 * um # diametre of dendrite
l_d     = 1743 * um # length of dendrite
area_d  = pi * d_d * l_d # total area of dendrite (tube)
g_Ld    = 0.00001 * siemens/cm**2 * area_d
E_Ld    = -87. * mV
Cd      = 2.5 * uF/cm**2 * area_d

# Soma
d_s = 10. * um # diametre of soma
l_s = 10. * um # length of soma
area_s = pi * d_s * l_s # total area of soma (sphere)
g_Ls = 0.00003 * siemens/cm**2 * area_s
E_Ls = -87. * mV
Cs = 1. * uF/cm**2 * area_s

tw = Cs / g_Ls

# Global parameters
E_Na = 120. * mV
g_Na = 0. * nS #msiemens/cm ** 2 Default: 1.2
alpha = 2. * nS
beta = 0.0045 * nA #0.0045
tauw = 45. * ms
v_thres = 0. * mV
v_reset = - 74. * mV 
delta_T = 2. * mV
v_T = -56. * mV
gc = 10 * nS # coupling conductance for first dendrite
Iinj = 1. * nA
taum = 1. * ms

####### Model #######
eqs = '''
dvm/dt = (- g_Ls * (vm - E_Ls) + g_Ls * delta_T * exp((vm - v_T) / delta_T) - gc * (vm - vd) - w + I_s) / Cs : volt (unless refractory)
dw/dt = (alpha * (vm - E_Ls) - w) / tauw : amp

dvd/dt = (-g_Ld * (vd - E_Ld) - gc * (vd - vm) + I_d + I_Na)/ Cd : volt

I_Na = - g_Na * m * (vd - E_Na) : amp
dm/dt = (minf - m) / taum : 1
minf = 1 / ( 1 + 100 * exp( -25 - vd /mV )) : 1

I_s : amp
I_d : amp
'''

neuron = NeuronGroup(1, model = eqs, threshold = 'vm > v_thres', reset = "vm = v_reset; w += beta", refractory = 2 * ms, method = "euler")

neuron.vm = E_Ls
neuron.vd = E_Ld
neuron.m = 0

trace = StateMonitor(neuron, ('vm', 'vd', 'm'), record = 0)
spk = SpikeMonitor(neuron)

run(duration_no_current1)
neuron.I_d = Iinj
run(duration_current)
neuron.I_d = 0 * nA
run(duration_no_current2)

# Nicer spikes (as per example of Gersner in brian2)
vm = trace[0].vm[:]
for t in spk.t:
    i = int(t / defaultclock.dt)
    vm[i] = 20*mV

# Plots
plt.figure(1)
plt.plot(trace.t / ms, vm / mV)
plt.plot(trace.t / ms, trace[0].vd[:] / mV)

plt.xlabel('time (ms)')
plt.ylabel('membrane potential (mV)')
plt.legend(['soma', 'apical trunk'])

plt.figure(2)
plt.plot(trace.t, trace[0].m[:])

plt.xlabel('time (ms)')
plt.ylabel('values')
plt.legend('m')

plt.show()