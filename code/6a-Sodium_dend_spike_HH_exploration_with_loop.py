#!/usr/bin/env python3

######################################################
#                                                    #
#   Code taken from Brete and used for parametric    #
#     exploration of HH model in order to create     #
#              dendritic sodium spikes.              #
#         No AdEx Integrate-and-fire here.           #
#                                                    #
######################################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as np

# List Needed loop to create graphs for Dendritic/somatic voltage peak and Na current 
# in accordance to external current input. Otherwise use steady I_ext_cont
I_ext_cont = list(np.arange(0, 2.0, 0.05))
#I_ext_cont = [1.2]

INa_ALL    = []
Voltage1   = []
Voltage2   = []

for i in I_ext_cont:
    
    start_scope() # Resets values at the beginning of each loop.
    
    ### neuronal Parameters ###
    ## Somatic (s) & dendritic (d) ##
    areas    = pi * 10 * um * 10 * um #2000*umetre**2
    aread    = pi * 1800 * um * 1.0 * um
    
    Cms      = (1 * ufarad*cm**-2) * areas
    gls      = 1 * (5e-5*siemens*cm**-2) * areas
    
    Cmd      = (1.0 * ufarad*cm**-2) * aread
    gld      = 1.5 * (5e-5*siemens*cm**-2) * aread
    
    gcs      = 10 * 0.00002 * siemens*cm**-2 * aread
    gcd      = 0.00002 * siemens*cm**-2 * areas # 1 * (5e-5*siemens*cm**-2) * area # goes with 'threshold = vs > *'
   
    El      = -80 * mV # Changes where the spike graphs starts. Default: -60
    EK      = -90 * mV # spike min to lower values.
    ENa     = 15 * mV # it designates where the peak will be. In addition with k**2?
    g_na    = (1.8*100 *msiemens*cm**-2) * aread
    g_kd    = (1.8*30 *msiemens*cm**-2) * aread # spike min to lower values with high g_kd
    VT      = -53 * mV
    v_reset = (-74. + 0) *mV
    
    delta_T = 2. * mV
    alpha = 2.5 * nS
    tauw  = 45 * ms
    beta  = 0.045 * nA
    
    ### Model parameters ###
    
    # Notes on how parameters change: 
    # k**2 -> at +2 moves spike to the right. at -{*} increased spike #. 
    # k**1 -> controls width/sharpness. ^ values -> sharp. low values -> wide.
    # k**3 -> changes frequency of spikes. ^ values -> more spikes.
    
    # K gates 
    kan1 = 0.032 * (mV**-1) * 1.1
    kan2 = (-18 + -10) * mV #Default: 15*mV + VT
    kan3 = (5 + -0) *mV 
    
    kbn1 = .5 * 1.1
    kbn2 = (-43 + -10) * mV # Default: 10*mV + VT
    kbn3 = (40 + -0) * mV
    
    # Na activation 
    kam1 = 0.32*(mV**-1) * 1.1
    kam2 = (-40 + 12) * mV # Default: 13*mV+VT. This is the threshold!!!!!
    kam3 = (4 + -0) * mV
    
    kbm1 = 0.28 * (mV**-1) * 1.1
    kbm2 = (13 + -10) * mV # Default: -VT-40*mV. Range of curve?
    kbm3 = (5 + -0) * mV
    
    # Na deactivation 
    kah1 = 0.128 * 1.1
    kah2 = (-16 + -10) * mV # Default: 17*mV+VT
    kah3 = (18 + -0) * mV
    
    kbh1 = 4. * 1.1
    kbh2 = (-13 + -10) * mV # Default: 40*mV+VT
    kbh3 = (5 + -0) * mV
    
    ## The model
    
    # Starts working with dendritic spikes @ ~ Iext = 0.15 nA
    
    # In case I want to add AdEx
    # dvs/dt = (- gcs * (vs - El) + gls * delta_T * exp((vs - VT) / delta_T) - gc * (vs - v) - w / Cms : volt (unless refractory)
    
    eqs = Equations('''
        
        dvs/dt = ( -gls*(vs-El) - gcs*(vs-vd) - w ) /Cms : volt
        dw/dt = ( alpha * (vs - El) - w ) / tauw : amp
    
        dvd/dt = (-gld*(vd-El) - gcd*(vd-vs) - INa - IK + Iext)/Cmd : volt 
        
        INa = g_na*(m**3)*h*(vd-ENa) : amp
        IK = g_kd*(n**4)*(vd-EK) : amp
    
        dm/dt = alpha_m*(1-m)-beta_m*m : 1
        dn/dt = alpha_n*(1-n)-beta_n*n : 1
        dh/dt = alpha_h*(1-h)-beta_h*h : 1
    
    
        alpha_n = kan1*(-vd+kan2) / (exp((-vd+kan2)/(kan3))-1.)/ms : Hz
    
        beta_n  = kbn1*exp((-vd + kbn2)/(kbn3))/ms : Hz
    
    
        alpha_m = kam1*(-vd+kam2)/(exp((-vd+kam2)/(kam3))-1.)/ms : Hz
    
        beta_m  = kbm1*(vd+kbm2)/(exp((vd+kbm2)/(kbm3))-1)/ms : Hz
    
    
        alpha_h = kah1*exp((-vd+kah2)/(kah3))/ms : Hz
    
        beta_h  = kbh1/(1+exp((-vd+kbh2)/(kbh3)))/ms : Hz
    
        Iext: amp
        
        ''')
    
    neuron = NeuronGroup(1, model=eqs, threshold='vs > 10000*mV', reset = 'vs = v_reset; w += beta', refractory=3*ms, method='exponential_euler')
    
    # Initialization
    neuron.vd   = El
    neuron.vs  = El
    
    # Record variables and spikes. 
    trace = StateMonitor(neuron, ('vd','vs','INa','IK'), record = True) # Default: record=[0]
    spikes = SpikeMonitor(neuron, record = True) 
    run(10 * ms, report='text')
    neuron.Iext = i * nA
    run(50 * ms, report='text')
    neuron.Iext = 0.0 * nA
    run(100 * ms, report='text')
    
    # Storring all I_Na and voltages for different current injections.
    INa_ALL.append(max((abs(trace[0].INa / nA))))
    Voltage1.append(max(trace[0].vd / mV))
    Voltage2.append(max(trace[0].vs / mV))    
    
    #Now cometh the plots...
#    plt.figure(1)
#    plt.plot(trace.t / ms, trace[0].vd / mV)
#    plt.plot(trace.t / ms, trace[0].vs / mV)
#    plt.xlabel('t (ms)')
#    plt.ylabel('v (mV)')
#    plt.legend(['dendrite','soma'])
#    
#    plt.figure(2)
#    plt.plot(trace.t / ms, trace[0].INa / nA)
#    plt.plot(trace.t / ms, trace[0].IK / nA)
#    plt.xlabel('t (ms)')
#    plt.ylabel('I (nA)')
#    plt.legend(['INa', 'IK'])
#    plt.show()
    
#plt.figure(3)
#plt.plot(I_ext_cont, Voltage1, '.')
#plt.xlabel('I external [nA]')
#plt.ylabel('Dendritic voltage peak [mV]')

#plt.figure(4)
#plt.plot(I_ext_cont, Voltage2, '.')
#plt.xlabel('I external [nA]')
#plt.ylabel('Somatic voltage peak [mV]')


plt.figure(5)
plt.plot(I_ext_cont, INa_ALL, '.')
plt.xlabel('I external [nA]')
plt.ylabel('INa [nA]')
plt.legend(['dendrite'])
plt.show()
