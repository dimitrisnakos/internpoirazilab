#!/usr/bin/env python3


####### AdEx model with > 1 dendrites connected #######
####### to the soma at a GC neuron              #######

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

# Parameters

duration_no_current = 30 * ms
duration_current = 100 * ms

g_L = 30. * nS
E_L = -87. * mV
delta_T = 2. * mV
v_T = -56. * mV
C = 100. * pF
alpha = 2. * nS
taum = C / g_L
tauw = 45. * ms
v_thres = 0. * mV
v_reset = - 74. * mV 
beta = 0.045 * nA
gc = 10 * nS # coupling conductance for dendrite connected to soma
gc1 = 7 * nS # dend1 && dend2 connected to dend 
gc2 = 7 * nS
gc3 = 5 * nS # dend3 && dend4 connected to dend1

# Model
eqs = '''
dvd4/dt = (-g_L * (vd4 - E_L) - gc3 * (vd4 - vd1) + I) / C : volt
dvd3/dt = (-g_L * (vd3 - E_L) - gc3 * (vd3 - vd1)) / C : volt
dvd2/dt = (-g_L * (vd2 - E_L) - gc2 * (vd - vd2)) / C : volt
dvd1/dt = (-g_L * (vd1 - E_L) - gc1 * (vd1 - vd) - gc1 * (vd1 - vd3) - gc1 * (vd1 - vd4) - gc1 * (vd - vd1)) / C : volt
dvd/dt = (-g_L * (vd - E_L) - gc * (vd - vd2) - gc * (vd - vd1)) / C : volt
dvm/dt = (- g_L * (vm - E_L) + g_L * delta_T * exp((vm - v_T) / delta_T) - gc * (vm - vd) -w) / C : volt
dw/dt = (alpha * (vm - E_L) - w) / tauw : amp
I : amp
'''

neuron = NeuronGroup(1, model = eqs, threshold = 'vm > v_thres', reset = "vm = v_reset; w += beta", method = "euler")

neuron.vm = E_L
neuron.vd = E_L
neuron.vd1 = E_L
neuron.vd2 = E_L
neuron.vd3 = E_L
neuron.vd4 = E_L

trace = StateMonitor(neuron, ('vm', 'vd', 'vd1', 'vd2', 'vd3', 'vd4'), record = 0)
spk = SpikeMonitor(neuron)

run(duration_no_current)
neuron.I = 150 * nA
run(duration_current)
neuron.I = 0 * nA
run(duration_no_current)

# Nicer spikes (as per example of Gersner in brian2)
vm = trace[0].vm[:]
for t in spk.t:
    i = int(t / defaultclock.dt)
    vm[i] = 20 * mV 

# Plots
plt.plot(trace.t / ms, vm / mV)
plt.plot(trace.t / ms, trace[0].vd[:] / mV)
plt.plot(trace.t / ms, trace[0].vd1[:] / mV)
plt.plot(trace.t / ms, trace[0].vd2[:] / mV)
plt.plot(trace.t / ms, trace[0].vd3[:] / mV)
plt.plot(trace.t / ms, trace[0].vd4[:] / mV)

plt.xlabel('time (ms)')
plt.ylabel('membrane potential (mV)')
plt.legend(['soma', 'apical trunk', 'distal 1', 'distal 2', 'distal 3', 'distal 4'])
plt.show();
